#include <random>
#include <string>
#include <time.h>
#include <unordered_map>

#include <SDL.h>
#include <SDL_ttf.h>


constexpr int SCREEN_WIDTH = 1024;
constexpr int SCREEN_HEIGHT = 768;

struct Game {
    enum class State {
        QUIT = 0,
        IN_MENU = 1,
        RUNNING = 2,
        INSTRUCTIONS = 3,
        UPDATING = 4
    };

    State state = State::IN_MENU;
    bool hasStarted = false;
    uint32_t steps = 0;

    static const int FIELD_WIDTH = 4;
    static const int FIELD_HEIGHT = 4;
    uint8_t field[FIELD_WIDTH*FIELD_HEIGHT];
    uint8_t statusField[FIELD_WIDTH*FIELD_HEIGHT];
};

struct Texture {
    const int width;
    const int height;
    SDL_Texture* ptr;
};

class TextureManager {
private:
    std::unordered_map<std::string, Texture> textures_;
    std::vector<SDL_Texture*> sdlTextures_;

public:
    void LoadTexureFromSurface(const std::string& alias, SDL_Renderer* renderer, SDL_Surface* surface) {
        SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, surface);
        sdlTextures_.push_back(texture);
        textures_.insert(std::make_pair(alias, Texture{ surface->w, surface->h, texture }));
    }

    const Texture& GetTexture(const std::string& alias) const {
        return textures_.find(alias)->second;
    }

    TextureManager() = default;
    ~TextureManager() {
        for (const auto& texture : sdlTextures_) {
            SDL_DestroyTexture(texture);
        }
    }

    TextureManager(const TextureManager&) = delete;
    TextureManager& operator=(const TextureManager&) = delete;
};


void CreateTextTexture(TextureManager& manager, const std::string alias, SDL_Renderer* renderer, TTF_Font* font, const char* text, SDL_Color fg) {
    SDL_Surface* renderedText = TTF_RenderText_Blended(font, text, fg);
    manager.LoadTexureFromSurface(alias, renderer, renderedText);
    SDL_FreeSurface(renderedText);
}

void DrawTextureCentered(const std::string& alias, const TextureManager& textureManager, SDL_Renderer* renderer, int x, int y, int w, int h) {
    const Texture& texture = textureManager.GetTexture(alias);
    SDL_Rect renderQuad = {
        x + (w - texture.width) / 2,
        y + (h - texture.height) / 2,
        texture.width,
        texture.height
    };
    SDL_RenderCopy(renderer, texture.ptr, nullptr, &renderQuad);
}

void DrawTexture(const std::string& alias, const TextureManager& textureManager, SDL_Renderer* renderer, int x, int y) {
    const Texture& texture = textureManager.GetTexture(alias);
    SDL_Rect renderQuad = { x, y, texture.width, texture.height };
    SDL_RenderCopy(renderer, texture.ptr, nullptr, &renderQuad);
}

struct Button {
    enum class State {
        NORMAL,
        HOVERED,
        PRESSED,
        DISABLED
    };

    int x;
    int y;
    int width;
    int height;
    State state;
};

void DrawButton(const std::string& textAlias, const Button& button, const SDL_Color color, const TextureManager& textureManager, SDL_Renderer* renderer) {
    SDL_Rect rect = { button.x, button.y, button.width, button.height };

    SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a);
    SDL_RenderFillRect(renderer, &rect);
    DrawTextureCentered(textAlias, textureManager, renderer, button.x, button.y, button.width, button.height);
}

constexpr int MENU_BUTTON_WIDTH = 300;
constexpr int MENU_BUTTON_HEIGHT = 50;
constexpr int MENU_POS_X = (SCREEN_WIDTH - MENU_BUTTON_WIDTH) / 2;
constexpr int MENU_POS_Y = SCREEN_HEIGHT / 2 - 160;

static std::unordered_map<std::string, Button> MenuLayout = {
    { "CONTINUE",{ MENU_POS_X, MENU_POS_Y, MENU_BUTTON_WIDTH, MENU_BUTTON_HEIGHT, Button::State::DISABLED } },
    { "NEW_GAME",{ MENU_POS_X, MENU_POS_Y + MENU_BUTTON_HEIGHT + 40, MENU_BUTTON_WIDTH, MENU_BUTTON_HEIGHT } },
    { "HELP",{ MENU_POS_X, MENU_POS_Y + 2 * (MENU_BUTTON_HEIGHT + 40), MENU_BUTTON_WIDTH, MENU_BUTTON_HEIGHT } },
    { "EXIT",{ MENU_POS_X, MENU_POS_Y + 3 * (MENU_BUTTON_HEIGHT + 40), MENU_BUTTON_WIDTH, MENU_BUTTON_HEIGHT } }
};

static Button InstructionsBackButton = { MENU_POS_X, MENU_POS_Y + 3 * (MENU_BUTTON_HEIGHT + 40), MENU_BUTTON_WIDTH, MENU_BUTTON_HEIGHT };

const SDL_Color textColorNormal{ 0xff, 0xff, 0xff };
const SDL_Color textColorHovered{ 0xec, 0xe0, 0 };
const SDL_Color textColorPressed{ 0xec, 0, 0 };
const SDL_Color textColorDisabled{ 0x80, 0x80, 0x80 };
const SDL_Color buttonColorNormal{ 0x96, 0x96, 0x96 };
const SDL_Color buttonColorHovered{ 0xb8, 0xb8, 0xae };
const SDL_Color buttonColorPressed{ 0x66, 0x66, 0x66 };

void DrawMenu(SDL_Renderer* renderer, const TextureManager& textureManager) {
    for (const auto& element : MenuLayout) {
        std::string textureAlias = element.first;
        SDL_Color buttonColor = buttonColorNormal;
        switch (element.second.state) {
        case Button::State::HOVERED: textureAlias += "_HOVERED"; buttonColor = buttonColorHovered; break;
        case Button::State::PRESSED: textureAlias += "_PRESSED"; buttonColor = buttonColorPressed; break;
        case Button::State::DISABLED: textureAlias += "_DISABLED"; buttonColor = buttonColorNormal; break;
        }
        const auto& button = element.second;
        DrawButton(textureAlias, button, buttonColor, textureManager, renderer);
    }
}

const static std::pair<std::string, SDL_Color> FieldColorMap[9] = {
    std::make_pair("CELL1", SDL_Color{ 0xec, 0, 0x80 }),
    std::make_pair("CELL2", SDL_Color{ 0xec, 0, 0 }),
    std::make_pair("CELL4", SDL_Color{ 0xec, 0x80, 0 }),
    std::make_pair("CELL8", SDL_Color{ 0xd8, 0xb9, 0 }),
    std::make_pair("CELL16", SDL_Color{ 0x90, 0xcc, 0 }),
    std::make_pair("CELL32", SDL_Color{ 0x2f, 0x96, 0x33 }),
    std::make_pair("CELL64", SDL_Color{ 0x00, 0xec, 0xec }),
    std::make_pair("CELL128", SDL_Color{ 0x00, 0x2f, 0xff }),
    std::make_pair("CELL256", SDL_Color{ 0x80, 0x00, 0xec })
};

constexpr int CELL_SIZE = 80;
constexpr int CELL_SPACING = CELL_SIZE / 4;
constexpr int FIELD_WIDTH = (CELL_SIZE + CELL_SPACING) * Game::FIELD_WIDTH - CELL_SPACING;
constexpr int FIELD_HEIGHT = (CELL_SIZE + CELL_SPACING) * Game::FIELD_HEIGHT - CELL_SPACING;
constexpr int FIELD_POS_X = (SCREEN_WIDTH - FIELD_WIDTH) / 2;
constexpr int FIELD_POS_Y = (SCREEN_HEIGHT - FIELD_HEIGHT) / 2;
constexpr int ANIMATION_STEPS = 10;

void DrawGameField(SDL_Renderer* renderer, const TextureManager& textureManager, const Game& game) {
    int x = FIELD_POS_X;
    int y = FIELD_POS_Y;
    for (int i = 0; i < game.FIELD_HEIGHT; ++i) {
        x = FIELD_POS_X;
        for (int j = 0; j < game.FIELD_WIDTH; ++j) {
            const int idx = i * game.FIELD_HEIGHT + j;
            float shrinkage = (ANIMATION_STEPS - game.statusField[idx] * 0.5f) / (ANIMATION_STEPS);
            SDL_Rect rect = { x, y + static_cast<int>(CELL_SIZE * (1 - shrinkage * shrinkage) / 2), CELL_SIZE, static_cast<int>(CELL_SIZE * shrinkage * shrinkage) };
            const auto& fieldPair = FieldColorMap[game.field[idx]];
            const SDL_Color& color = fieldPair.second;
            const std::string& textAlias = fieldPair.first;
            SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a);
            SDL_RenderFillRect(renderer, &rect);
            DrawTextureCentered(textAlias, textureManager, renderer, x, y, CELL_SIZE, CELL_SIZE);

            x += CELL_SIZE + CELL_SPACING;
        }
        y += CELL_SIZE + CELL_SPACING;
    }
    if (!game.hasStarted) {
        DrawTextureCentered("WIN", textureManager, renderer, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    }
}

void DrawHelp(SDL_Renderer* renderer, const TextureManager& textureManager) {
    int posY = MENU_POS_Y;
    int posX = MENU_POS_X - 50;
    DrawTexture("INSTRUCTIONS0", textureManager, renderer, posX, posY);
    const auto& texture0 = textureManager.GetTexture("INSTRUCTIONS0");
    posY += texture0.height + 5;
    DrawTexture("INSTRUCTIONS1", textureManager, renderer, posX, posY);
    const auto& texture1 = textureManager.GetTexture("INSTRUCTIONS1");
    posY += texture1.height * 2 + 5;
    DrawTexture("INSTRUCTIONS2", textureManager, renderer, posX, posY);

    SDL_Color buttonColor = buttonColorNormal;
    std::string textureAlias = "BACK";
    switch (InstructionsBackButton.state) {
    case Button::State::HOVERED: textureAlias += "_HOVERED"; buttonColor = buttonColorHovered; break;
    case Button::State::PRESSED: textureAlias += "_PRESSED"; buttonColor = buttonColorPressed; break;
    }
    DrawButton(textureAlias, InstructionsBackButton, buttonColor, textureManager, renderer);
}

constexpr int UPDATING_TIMER_EVENT = 0x1;

Uint32 SdlTimerCallback(Uint32 interval, void* /*param*/) {
    SDL_Event event;
    SDL_UserEvent userEvent;

    userEvent.type = SDL_USEREVENT;
    userEvent.code = 0;
    userEvent.data1 = nullptr;
    userEvent.data2 = nullptr;

    event.type = SDL_USEREVENT;
    event.user = userEvent;

    SDL_PushEvent(&event);
    return interval;
}

void CreateNewGame(Game& game) {
    for (int i = 0; i < game.FIELD_HEIGHT; ++i) {
        for (int j = 0; j < game.FIELD_WIDTH; ++j) {
            const int idx = i * game.FIELD_HEIGHT + j;
            game.field[idx] = rand() % 9;
            game.statusField[idx] = 0;
        }
    }

    game.hasStarted = true;
}

void ProcessMenu(const SDL_Event& event, Game& game) {
    auto& continueButton = MenuLayout.find("CONTINUE")->second;
    if (game.hasStarted && continueButton.state == Button::State::DISABLED) {
        continueButton.state = Button::State::NORMAL;
    } else if (!game.hasStarted) {
        continueButton.state = Button::State::DISABLED;
    }
    switch (event.type) {
    case SDL_MOUSEMOTION:
    case SDL_MOUSEBUTTONDOWN:
    case SDL_MOUSEBUTTONUP:
        int mouseX, mouseY;
        SDL_GetMouseState(&mouseX, &mouseY);
        for (auto& element : MenuLayout) {
            if (element.second.state == Button::State::DISABLED) {
                continue;
            }
            if (mouseX >= element.second.x && mouseX <= element.second.x + element.second.width
                && mouseY >= element.second.y && mouseY <= element.second.y + element.second.height) {
                switch (event.type) {
                case SDL_MOUSEMOTION:
                    if (element.second.state != Button::State::PRESSED) {
                        element.second.state = Button::State::HOVERED;
                    }
                    break;
                case SDL_MOUSEBUTTONDOWN:
                    element.second.state = Button::State::PRESSED;
                    break;
                case SDL_MOUSEBUTTONUP:
                    element.second.state = Button::State::HOVERED;
                    if (element.first == "EXIT") {
                        game.state = Game::State::QUIT;
                    } else if (element.first == "NEW_GAME") {
                        CreateNewGame(game);
                        game.state = Game::State::RUNNING;
                    } else if (element.first == "HELP") {
                        game.state = Game::State::INSTRUCTIONS;
                    } else if (element.first == "CONTINUE") {
                        game.state = Game::State::RUNNING;
                    }
                    break;
                }
            } else {
                element.second.state = Button::State::NORMAL;
            }
        }
        break;
    case SDL_KEYDOWN:
        const uint8_t* currentKeyStates = SDL_GetKeyboardState(NULL);
        if (currentKeyStates[SDL_SCANCODE_ESCAPE]) {
            if (game.hasStarted) {
                game.state = Game::State::RUNNING;
            }
        }
        break;
    }
}

void IncreaseCell(Game& game, int x, int y) {
    if (x >= 0 && x < game.FIELD_WIDTH
        && y >= 0 && y < game.FIELD_HEIGHT) {
        const int idx = y * game.FIELD_HEIGHT + x;
        if (game.field[idx] < 8) {
            game.field[idx] += 1;
            game.statusField[idx] += ANIMATION_STEPS;
        }
    }
}

void DecreaseCell(Game& game, int x, int y) {
    if (x >= 0 && x < game.FIELD_WIDTH
        && y >= 0 && y < game.FIELD_HEIGHT) {
        const int idx = y * game.FIELD_HEIGHT + x;
        if (game.field[idx] > 0) {
            game.field[idx] -= 1;
            game.statusField[idx] += ANIMATION_STEPS;
        }
    }
}

void IncreaseCells(Game& gameState, int x, int y) {
    IncreaseCell(gameState, x, y);
    IncreaseCell(gameState, x + 1, y);
    IncreaseCell(gameState, x - 1, y);
    IncreaseCell(gameState, x, y + 1);
    IncreaseCell(gameState, x, y - 1);
}

void DecreaseCells(Game& gameState, int x, int y) {
    DecreaseCell(gameState, x, y);
    DecreaseCell(gameState, x + 1, y);
    DecreaseCell(gameState, x - 1, y);
    DecreaseCell(gameState, x, y + 1);
    DecreaseCell(gameState, x, y - 1);
}

bool IsWinCondition(Game& gameState) {
    for (int i = 0; i < gameState.FIELD_HEIGHT * gameState.FIELD_WIDTH; ++i) {
        if (gameState.field[i] != 5) {
            return false;
        }
    }
    return true;
}

constexpr int ANIMATION_TIMER_DELAY = 10;

void ProcessRunning(const SDL_Event& event, Game& game) {
    switch (event.type) {
    case SDL_MOUSEBUTTONUP:
        if (!game.hasStarted) {
            break;
        }
        int mouseX, mouseY;
        SDL_GetMouseState(&mouseX, &mouseY);
        if (mouseX >= FIELD_POS_X && mouseX <= FIELD_POS_X + FIELD_WIDTH
            && mouseY >= FIELD_POS_Y && mouseY <= FIELD_POS_Y + FIELD_HEIGHT) {
            const int mouseIdxJ = (mouseX - FIELD_POS_X) / (CELL_SIZE + CELL_SPACING);
            const int mouseIdxI = (mouseY - FIELD_POS_Y) / (CELL_SIZE + CELL_SPACING);
            if (mouseIdxJ * (CELL_SIZE + CELL_SPACING) + CELL_SIZE + FIELD_POS_X >= mouseX
                && mouseIdxI * (CELL_SIZE + CELL_SPACING) + CELL_SIZE + FIELD_POS_Y >= mouseY) {
                const int idx = mouseIdxI * game.FIELD_HEIGHT + mouseIdxJ;
                switch (event.button.button) {
                case SDL_BUTTON_LEFT:
                    IncreaseCells(game, mouseIdxJ, mouseIdxI);
                    ++game.steps;
                    if (IsWinCondition(game)) {
                        game.hasStarted = false;
                    }
                    game.state = Game::State::UPDATING;
                    break;
                case SDL_BUTTON_RIGHT:
                    DecreaseCells(game, mouseIdxJ, mouseIdxI);
                    ++game.steps;
                    if (IsWinCondition(game)) {
                        game.hasStarted = false;
                    }
                    game.state = Game::State::UPDATING;
                    break;
                }
            }
        }
        break;
    case SDL_KEYDOWN:
        const uint8_t* currentKeyStates = SDL_GetKeyboardState(NULL);
        if (currentKeyStates[SDL_SCANCODE_ESCAPE]) {
            game.state = Game::State::IN_MENU;
        }
        break;
    }
}

void ProcessUpdating(const SDL_Event& event, Game& game) {
    bool isUpdatingEnded = false;
    switch (event.type) {
    case SDL_USEREVENT:
        isUpdatingEnded = true;
        switch (event.user.type) {
        case SDL_USEREVENT:
            for (int i = 0; i < game.FIELD_HEIGHT * game.FIELD_WIDTH; ++i) {
                if (game.statusField[i] > 0) {
                    game.statusField[i] -= 1;
                }
                if (game.statusField[i] > 0) {
                    isUpdatingEnded = false;
                }
            }
            break;
        }
        break;
    }
    if (isUpdatingEnded) {
        game.state = Game::State::RUNNING;
    }
}

void ProcessHelp(const SDL_Event& event, Game& game) {
    switch (event.type) {
    case SDL_MOUSEMOTION:
    case SDL_MOUSEBUTTONDOWN:
    case SDL_MOUSEBUTTONUP:
        int mouseX, mouseY;
        SDL_GetMouseState(&mouseX, &mouseY);
        if (mouseX >= InstructionsBackButton.x && mouseX <= InstructionsBackButton.x + InstructionsBackButton.width
            && mouseY >= InstructionsBackButton.y && mouseY <= InstructionsBackButton.y + InstructionsBackButton.height) {
            switch (event.type) {
            case SDL_MOUSEMOTION:
                if (InstructionsBackButton.state != Button::State::PRESSED) {
                    InstructionsBackButton.state = Button::State::HOVERED;
                }
                break;
            case SDL_MOUSEBUTTONDOWN:
                InstructionsBackButton.state = Button::State::PRESSED;
                break;
            case SDL_MOUSEBUTTONUP:
                InstructionsBackButton.state = Button::State::HOVERED;
                game.state = Game::State::IN_MENU;
                break;
            }
        } else {
            InstructionsBackButton.state = Button::State::NORMAL;
        }
        break;
    case SDL_KEYDOWN:
        const uint8_t* currentKeyStates = SDL_GetKeyboardState(NULL);
        if (currentKeyStates[SDL_SCANCODE_ESCAPE]) {
            game.state = Game::State::IN_MENU;
        }
        break;
    }
}



int main(int argc, char* argv[]) {
    SDL_Window* window = nullptr;
    SDL_Renderer* renderer = nullptr;

    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER) < 0) {
        return 1;
    }

    if (TTF_Init() < 0) {
        return 1;
    }

    window = SDL_CreateWindow("SimpleSDLPuzzle", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);

    if (!window) {
        return 1;
    }

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

    if (!renderer) {
        return 1;
    }

    SDL_SetRenderDrawColor(renderer, 0x0, 0x0, 0x0, 0xff);
    SDL_UpdateWindowSurface(window);

    SDL_Event event;

    time_t t;
    time(&t);
    srand(static_cast<unsigned int>(t));
    Game game;

    TextureManager textureManager;

    TTF_Font* font = TTF_OpenFont("CLBSWFT0.ttf", 42);

    CreateTextTexture(textureManager, "CONTINUE", renderer, font, "Continue", textColorNormal);
    CreateTextTexture(textureManager, "CONTINUE_HOVERED", renderer, font, "Continue", textColorHovered);
    CreateTextTexture(textureManager, "CONTINUE_PRESSED", renderer, font, "Continue", textColorPressed);
    CreateTextTexture(textureManager, "CONTINUE_DISABLED", renderer, font, "Continue", textColorDisabled);
    CreateTextTexture(textureManager, "NEW_GAME", renderer, font, "New Game", textColorNormal);
    CreateTextTexture(textureManager, "NEW_GAME_HOVERED", renderer, font, "New Game", textColorHovered);
    CreateTextTexture(textureManager, "NEW_GAME_PRESSED", renderer, font, "New Game", textColorPressed);
    CreateTextTexture(textureManager, "EXIT", renderer, font, "Exit", textColorNormal);
    CreateTextTexture(textureManager, "EXIT_HOVERED", renderer, font, "Exit", textColorHovered);
    CreateTextTexture(textureManager, "EXIT_PRESSED", renderer, font, "Exit", textColorPressed);
    CreateTextTexture(textureManager, "HELP", renderer, font, "Instructions", textColorNormal);
    CreateTextTexture(textureManager, "HELP_HOVERED", renderer, font, "Instructions", textColorHovered);
    CreateTextTexture(textureManager, "HELP_PRESSED", renderer, font, "Instructions", textColorPressed);
    CreateTextTexture(textureManager, "BACK", renderer, font, "Back", textColorNormal);
    CreateTextTexture(textureManager, "BACK_HOVERED", renderer, font, "Back", textColorHovered);
    CreateTextTexture(textureManager, "BACK_PRESSED", renderer, font, "Back", textColorPressed);

    CreateTextTexture(textureManager, "CELL1", renderer, font, "1", textColorNormal);
    CreateTextTexture(textureManager, "CELL2", renderer, font, "2", textColorNormal);
    CreateTextTexture(textureManager, "CELL4", renderer, font, "4", textColorNormal);
    CreateTextTexture(textureManager, "CELL8", renderer, font, "8", textColorNormal);
    CreateTextTexture(textureManager, "CELL16", renderer, font, "16", textColorNormal);
    CreateTextTexture(textureManager, "CELL32", renderer, font, "32", textColorNormal);
    CreateTextTexture(textureManager, "CELL64", renderer, font, "64", textColorNormal);
    CreateTextTexture(textureManager, "CELL128", renderer, font, "128", textColorNormal);
    CreateTextTexture(textureManager, "CELL256", renderer, font, "256", textColorNormal);

    TTF_CloseFont(font);
    font = TTF_OpenFont("CLBSWFT0.ttf", 24);

    CreateTextTexture(textureManager, "INSTRUCTIONS0", renderer, font,
        "Use left and right mouse button to increase or",
        textColorNormal);
    CreateTextTexture(textureManager, "INSTRUCTIONS1", renderer, font,
        "decrease numbers on the field",
        textColorNormal);
    CreateTextTexture(textureManager, "INSTRUCTIONS2", renderer, font,
        "Fill the field with number 32",
        textColorNormal);

    TTF_CloseFont(font);
    font = TTF_OpenFont("CLBSWFT0.ttf", 96);

    CreateTextTexture(textureManager, "WIN", renderer, font, "You Win", textColorNormal);

    TTF_CloseFont(font);

    SDL_AddTimer(ANIMATION_TIMER_DELAY, SdlTimerCallback, nullptr);

    while (game.state != Game::State::QUIT) {
        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_QUIT) {
                game.state = Game::State::QUIT;
            }
            switch (game.state) {
            case Game::State::IN_MENU: ProcessMenu(event, game); break;
            case Game::State::RUNNING: ProcessRunning(event, game); break;
            case Game::State::UPDATING: ProcessUpdating(event, game); break;
            case Game::State::INSTRUCTIONS: ProcessHelp(event, game); break;
            }
        }

        SDL_SetRenderDrawColor(renderer, 0x0, 0x0, 0x0, 0xff);
        SDL_RenderClear(renderer);

        switch (game.state) {
        case Game::State::IN_MENU: DrawMenu(renderer, textureManager); break;
        case Game::State::RUNNING: DrawGameField(renderer, textureManager, game); break;
        case Game::State::UPDATING: DrawGameField(renderer, textureManager, game); break;
        case Game::State::INSTRUCTIONS: DrawHelp(renderer, textureManager); break;
        }

        SDL_RenderPresent(renderer);
    }

    TTF_Quit();

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
}
